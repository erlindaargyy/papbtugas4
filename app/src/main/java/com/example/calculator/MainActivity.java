package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0;
    Button bttambah, btkali, btkurang, btbagi, bthasil, btclear, btkoma;
    TextView angka;
    float Bil1, Bil2;
    boolean Tambah, Kurang, Kali, Bagi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        bt3 = findViewById(R.id.bt3);
        bt4 = findViewById(R.id.bt4);
        bt5 = findViewById(R.id.bt5);
        bt6 = findViewById(R.id.bt6);
        bt7 = findViewById(R.id.bt7);
        bt8 = findViewById(R.id.bt8);
        bt9 = findViewById(R.id.bt9);
        bt0 = findViewById(R.id.bt0);
        bttambah = findViewById(R.id.bttambah);
        btkali = findViewById(R.id.btkali);
        btkurang = findViewById(R.id.btkurang);
        btbagi = findViewById(R.id.btbagi);
        bthasil = findViewById(R.id.bthasil);
        btclear = findViewById(R.id.btC);
        btkoma = findViewById(R.id.btkoma);
        angka = findViewById(R.id.angka);


        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "1");
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "2");
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "3");
            }
        });
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "4");
            }
        });
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "5");
            }
        });
        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "6");
            }
        });
        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "7");
            }
        });
        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "8");
            }
        });
        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "9");
            }
        });
        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + "0");
            }
        });

        btclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText("");
            }
        });
        btkoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka.setText(angka.getText() + ".");
            }
        });

        bttambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (angka == null) {
                    angka.setText("");
                } else {
                    Bil1 = Float.parseFloat(angka.getText() + "");
                    Tambah = true;
                    angka.setText(null);
                }
            }
        });
        btkurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Kurang = true;
                angka.setText(null);
            }
        });
        btbagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Bagi = true;
                angka.setText(null);
            }
        });
        btkali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Kali = true;
                angka.setText(null);
            }
        });

        bthasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                String message2 = null;
                Bil2 = Float.parseFloat(angka.getText() + "");
                if (Tambah == true) {
                    angka.setText(Bil1 + Bil2 + "");
                    message2 = String.valueOf(Bil1+ "+"+ Bil2);
                    Tambah = false;
                }
                if (Kurang == true) {
                    angka.setText(Bil1 - Bil2 + "");
                    message2 = String.valueOf(Bil1+ "-"+ Bil2);
                    Kurang = false;
                }
                if (Bagi == true) {
                    angka.setText(Bil1 / Bil2 + "");
                    message2 = String.valueOf(Bil1+ "/"+ Bil2);
                    Bagi = false;
                }
                if (Kali == true) {
                    angka.setText(Bil1 * Bil2 + "");
                    message2 = String.valueOf(Bil1+ "*"+ Bil2);
                    Kali = false;
                }
                String message = angka.getText().toString();
                intent.putExtra("a", message);
                intent.putExtra("b", message2);
                startActivity(intent);

            }
        });
    }
}

